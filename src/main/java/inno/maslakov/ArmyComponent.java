package inno.maslakov;

import java.util.ArrayList;

/**
 * Created by sa on 08.03.17.
 */
public abstract class ArmyComponent {
    protected ArrayList<ArmyComponent> armys = new ArrayList<ArmyComponent>();
    protected int cntSoldiers;
    protected int ammos = 0;
    protected float money = 0;

    public ArmyComponent() {
        cntSoldiers = 0;
    }

    public ArmyComponent(int ammos){
        this.ammos = ammos;
    }

    public ArmyComponent(float money) {
        this.money = money;
    }

    public int sumSoldiers(){
        int sum = 0;
        for(ArmyComponent armyComponent: armys) {
            sum += armyComponent.sumSoldiers();
        }
        return sum + cntSoldiers;
    }
    public int sumAmmo(){
        int sum = 0;
        for(ArmyComponent armyComponent: armys){
            sum += armyComponent.sumAmmo();
        }
        return sum + ammos;
    }
    public float sumMoney(){
        float sum = 0;
        for(ArmyComponent armyComponent: armys){
            sum += armyComponent.sumMoney();
        }
        return sum + money;
    }
    public void add(ArmyComponent armyComponent){
        armys.add(armyComponent);
    }
}
