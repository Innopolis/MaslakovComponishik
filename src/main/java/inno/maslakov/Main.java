package inno.maslakov;

/**
 * Created by sa on 08.03.17.
 */
public class Main {
    public static void main(String[] args) {
        Platoon platoon1 = new Platoon(2);
        Platoon platoon2 = new Platoon(2);
        System.out.printf("platoon1 = %d, platoon2 = %d\n",
                platoon1.sumSoldiers(),
                platoon2.sumSoldiers());

        Troop troop1 = new Troop(100);
        troop1.add(platoon1);
        troop1.add(platoon2);
        Troop troop2 = new Troop(200);
        troop2.add(platoon1);
        troop2.add(platoon2);
        System.out.printf("troop1 = %d, troop2 = %d\n",
                troop1.sumSoldiers(),
                troop2.sumSoldiers());
        Battalion battalion1 = new Battalion();
        battalion1.add(troop1);
        battalion1.add(troop2);
        Battalion battalion2 = new Battalion();
        battalion2.add(troop1);
        battalion2.add(troop2);
        System.out.printf("battalion1 = %d, battalion2 = %d\n",
                battalion1.sumSoldiers(),
                battalion2.sumSoldiers());

        MilitaryUnit militaryUnit1 = new MilitaryUnit(500);
        militaryUnit1.add(battalion1);
        militaryUnit1.add(battalion2);
        MilitaryUnit militaryUnit2 = new MilitaryUnit(1000);
        militaryUnit2.add(battalion1);
        militaryUnit2.add(battalion2);

        MilitaryDistrict militaryDistrict = new MilitaryDistrict();
        militaryDistrict.add(militaryUnit1);
        militaryDistrict.add(militaryUnit2);

        Army army = new Army();
        army.add(militaryDistrict);
        army.add(militaryDistrict);
        System.out.println(army.sumMoney());

    }
}
